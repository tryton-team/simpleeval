simpleeval (1.0.3-1) unstable; urgency=medium

  * Adding upstream version 1.0.3.
  * Add python3-hatchling to BuildDepends.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 06 Nov 2024 12:56:33 +0100

simpleeval (1.0.0-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.7.0, no changes needed.
  * Merging upstream version 1.0.0 (Closes: #1084107).
  * Remove Depends on python3-pkg-resources (Closes: #1083769).

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 09 Oct 2024 11:35:53 +0200

simpleeval (0.9.13-1) unstable; urgency=medium

  * Add a salsa-ci.yml
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Replace use of deprecated $ADTTMP with $AUTOPKGTEST_TMP.
  * Bump the Standards-Version to 4.6.2, no changes needed.
  * Do not take into account python package metadata when building twice
    (Closes: #1047962).
  * Merging upstream version 0.9.13.
  * Update year of debian copyright.
  * Update d/copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 03 Jul 2024 09:49:53 +0200

simpleeval (0.9.12-1) unstable; urgency=medium

  * Merging upstream version 0.9.12.
  * Bump to Standards-Version 4.6.1.0, no changes needed.
  * Bump d/copyright year.
  * Remove the 01-PEP517.patch in favor of the new pybuild-plugin-
    pyproject plugin.
  * Use pybuild-plugin-pyproject as build system.
  * Remove duplicate license.
  * Use debhelper-compat (= 13).

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 09 Sep 2022 20:29:00 +0200

simpleeval (0.9.11-1) unstable; urgency=medium

  * Updating to standards version 4.5.1, no changes needed.
  * Point the download URL to Pypi, the old URL is not working any more.
  * Merging upstream version 0.9.11 (Closes: #997606).
  * Add 01-PEP517.patch to build the now setup.py-less package.
  * Replace deprecated $ADTTMP with $AUTOPKGTEST_TMP.
  * Bump to Standards-Version 4.6.0.1, no changes needed.
  * Add Rules-Requires-Root: no in d/control.
  * Bump debian copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 26 Oct 2021 09:46:39 +0200

simpleeval (0.9.10-1) unstable; urgency=medium

  * Merging upstream version 0.9.10 (Closes: #950852).

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 10 Feb 2020 11:10:45 +0100

simpleeval (0.9.6-2) unstable; urgency=medium

  * Updating to Standards-Version 4.4.0, no changes needed.
  * Drop the Python 2 support (Closes: #938482).
  * Bump the debian copyright year.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 30 Aug 2019 21:19:01 +0200

simpleeval (0.9.6-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.1.1, no changes needed.
  * Update year of debian copyright.
  * Updating to standards version 4.1.3, no changes needed.
  * control: update Vcs-Browser and Vcs-Git
  * Set the Maintainer address to team+tryton-team@tracker.debian.org.
  * Merging upstream version 0.9.6.
  * Updating to standards version 4.2.1, no changes needed.
  * Remove obsolete X-Python-Version: >= 2.6.
  * Bump debhelper compat to 11.
  * Install the package test script in /usr/share/<pkg_name>.
  * Add an autopkgtest test suite.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 22 Nov 2018 13:52:30 +0100

simpleeval (0.9.5-1) unstable; urgency=medium

  * Change the maintainer address to tryton-debian@lists.alioth.debian.org
    (Closes: #865109).
  * Use the preferred https URL format in the copyright file.
  * Bump the Standards-Version to 4.0.1.
  * Bump the Standards-Version to 4.1.0, no changes needed.
  * Merging upstream version 0.9.5.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 04 Sep 2017 16:23:10 +0200

simpleeval (0.9.3-1) unstable; urgency=medium

  * Merging upstream version 0.9.3.
  * Update years in d/copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 24 Jan 2017 17:15:09 +0100

simpleeval (0.9.1-2) unstable; urgency=medium

  * Update years in d/copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 01 Dec 2016 23:37:19 +0100

simpleeval (0.9.1-1) unstable; urgency=medium

  * Updating to Standards-Version: 3.9.8, no changes needed.
  * Use releases directly from github rather from pypi.
  * Merging upstream version 0.9.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 01 Dec 2016 23:20:13 +0100

simpleeval (0.8.7-2) unstable; urgency=medium

  * Updating to standards version 3.9.7, no changes needed.
  * Updating VCS-Browser to https and canonical cgit URL.
  * Updating VCS-Git to https and canonical cgit URL.
  * Removing the braces from the dh call.
  * Removing the version constraint from python.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 08 Mar 2016 18:54:05 +0100

simpleeval (0.8.7-1) unstable; urgency=medium

  * Merging upstream version 0.8.7.
  * Updating d/copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 09 Nov 2015 11:13:30 +0100

simpleeval (0.8.5-1) unstable; urgency=medium

  * Disabling failing test for python3.2.
  * Adapting section naming in gbp.conf to current git-buildpackage.
  * Merging upstream version 0.8.5.
  * Removing all patches, went upstream.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 30 Jun 2015 11:33:01 +0200

simpleeval (0.8.2-2) unstable; urgency=medium

  * Disabling temporarily tests for failing test_string_length.
  * Adding 02-add-test-fix.
  * Re-enabling build tests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 27 Apr 2015 14:58:56 +0200

simpleeval (0.8.2-1) unstable; urgency=low

  * Initial packaging (Closes: #783188).

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 23 Apr 2015 19:25:39 +0200
